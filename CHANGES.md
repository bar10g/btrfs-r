# Changes in 0.2.0

-   Do not create a target directory

    This is to better match the behavior of btrfs-tools as they won't create
    a target directoy either.

# Changes in 0.1.0

Initial release
